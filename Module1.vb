﻿Imports System.Net
Imports System.IO

'
' THIS CODE IS INTENDED FOR DEMONSTRATION PURPOSES ONLY
' IT DOES NOT DEMONSTRATE PROPER ERROR HANDLING AND SHOULD
' NOT BE CONSIDERED PRODUCTION READY!
'
' THIS CODE IS PROVIDED "AS IS" AND WITHOUT WARANTY, IT'S
' USE IS AT YOUR OWN RISK.

Module Module1
    Private _url As String
    Private _un As String
    Private _pw As String
    Private _t As String = Nothing

    Private _email As String

    Private _userRecord As XDocument

    Private _uid As Integer

    Sub Main()
        Console.WriteLine("Calling login: ")


        ' SCHOOL URL: should be in the form http://schoolname.myschoolapp.com/
        _url = ""

        ' User Credentials: this user should be both a Platform Manager
        '                   as well as a Web Service Manager
        _un = ""
        _pw = ""

        'User id of the sample user record we will be pulling, updating and then pulling again.
        _uid = 0

        ' The new e-mail address to assign to the user.
        _email = ""

        If _url.Chars(_url.Length - 1) <> "/" Then
            _url = _url & "/"
        End If

        Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
        Console.WriteLine("=         Login Request       =")
        Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")

        ApiLogon()

        Console.WriteLine("Token: " & _t)

        Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
        Console.WriteLine("=       User Record Get       =")
        Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")

        ApiUserGet()

        Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
        Console.WriteLine("=     User Record Update      =")
        Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")

        ApiUserUpdate()

        Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
        Console.WriteLine("=       User Record Get       =")
        Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")

        ApiUserGet()

        Console.WriteLine("Finished")

    End Sub

    Sub ApiLogon()
        Dim request As WebRequest = Nothing
        Dim response As WebResponse = Nothing
        Dim xDoc As XDocument = Nothing

        Try

            Dim srvQuery As String = _url & "api/authentication/login?username=" & _un & "&password=" & _pw & "&format=xml"
            Console.WriteLine("Calling: " & srvQuery)
            request = WebRequest.Create(srvQuery)
            response = request.GetResponse()
            xDoc = XDocument.Load(response.GetResponseStream())

            Dim element As XElement = xDoc.Descendants("Token").First()

            If Not IsNothing(element) Then
                _t = element.Value
            End If

        Catch ex As Exception
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
            Console.WriteLine("=         LOGIN ERROR!        =")
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
            Console.WriteLine("Msg: " & ex.Message)
            Console.WriteLine("Src: " & ex.Source)
            Console.WriteLine("Stk: " & ex.StackTrace)
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
            Console.WriteLine("=        FINISH ERROR!        =")
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
        Finally
            If Not IsNothing(response) Then
                response.Close()
            End If

        End Try
    End Sub

    Sub ApiUserGet()
        Dim request As WebRequest = Nothing
        Dim response As WebResponse = Nothing

        Try
            Dim srvQuery As String = _url & "api/user/" & _uid & "/?t=" & _t & "&format=xml"
            request = WebRequest.Create(srvQuery)
            response = request.GetResponse()
            _userRecord = XDocument.Load(response.GetResponseStream())
            Console.WriteLine("Calling: " & srvQuery)
            Console.WriteLine(_userRecord)

            response.Close()
        Catch ex As Exception
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
            Console.WriteLine("=       USER GET ERROR!       =")
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
            Console.WriteLine("Msg: " & ex.Message)
            Console.WriteLine("Src: " & ex.Source)
            Console.WriteLine("Stk: " & ex.StackTrace)
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
            Console.WriteLine("=        FINISH ERROR!        =")
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
        Finally
            If Not IsNothing(response) Then
                response.Close()
            End If

        End Try

    End Sub

    Sub ApiUserUpdate()

        Dim element As XElement
        Dim url As String = _url & "api/user/" & _uid & "?t=" & _t & "&saveEmail=" & (Not String.IsNullOrEmpty(_email)) & "&format=xml"
        Dim request As WebRequest = WebRequest.Create(url)
        Dim content() As Byte
        Dim data As Stream
        Dim response As WebResponse

        Try
            Console.WriteLine("Calling: " & url)

            ' For simplicity sake we are just going to reuse the UserXML
            ' Record we grabbed from the get call, modify it and send it
            ' Back to the update call with a new e-mail address
            element = _userRecord.Descendants("Email").First()

            element.Value = _email

            content = Text.Encoding.UTF8.GetBytes(_userRecord.ToString())

            request.Method = "PUT"
            request.ContentType = "application/xml"
            request.ContentLength = content.Count()
            data = request.GetRequestStream()

            data.Write(content, 0, content.Count())

            data.Close()

            response = request.GetResponse()

            Console.WriteLine(XDocument.Load(response.GetResponseStream()))

            response.Close()
        Catch ex As Exception
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
            Console.WriteLine("=      USER UPDATE ERROR!     =")
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
            Console.WriteLine("Msg: " & ex.Message)
            Console.WriteLine("Src: " & ex.Source)
            Console.WriteLine("Stk: " & ex.StackTrace)
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
            Console.WriteLine("=        FINISH ERROR!        =")
            Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
        Finally
            If Not IsNothing(response) Then
                response.Close()
            End If

        End Try
    End Sub

End Module
